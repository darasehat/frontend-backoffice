// import axios from 'axios'
import router from 'vue-router';
// export default axios.create({
//   baseURL: process.env.apiBaseUrl
// })

export default function ({ $axios, redirect }) {
  $axios.onRequest(config => {
    console.log('Making request to ' + config.url)
  })

  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status)
    if (code === 400) {
      redirect('/400')
    }
    // if (code === 401) {
    //   console.log("401 nih")
      // router.push('/login');
      // auth.logout()
      // store.dispatch('logout');
    // }
  })

  $axios.setHeader('Content-Type', 'application/json');
  // $axios.setToken($store.state.api_token, 'Bearer')
}