import { extend } from "vee-validate";
import { required, email, min, max, min_value, numeric } from "vee-validate/dist/rules";
// import VueI18n from 'vue-i18n';
// import { i18n } from "./i18n";

// const i18n = new VueI18n({
//   locale: 'id',
//   en: {
//     validations: {
//       required: '{_field_} is required',
//   		email: '{_field_} must be filled with valid email format (ie: email@domain.com)'
//     }
//   },
//   id: {
// 		validations: {
//   		required: '{_field_} harus diisi',
//   		email: '{_field_} harus diisi dengan format email yang benar (ie: email@domain.com)'
// 	  }
//   }
// });

// configure({
//   // this will be used to generate messages.
//   defaultMessage: (field, values) => {
//     values._field_ = i18n.t(`fields.${field}`);
//     return i18n.t(`validations.${values._rule_}`, values);
//   }
// });

// localize({
//   en: {
//     messages: {
//       required: '{_field_} is required',
//   		email: '{_field_} must be filled with valid email format (ie: email@domain.com)'
//       // min: 'this field must have no less than {length} characters',
//       // max: (_, { length }) => `this field must have no more than ${length} characters`
//     }
//   },
//   id: {
//   	messages: {
//   		required: '{_field_} harus diisi',
//   		email: '{_field_} harus diisi dengan format email yang benar (ie: email@domain.com)'
//   	}
//   }
// });

extend("required", required)
// extend("required", {
//   ...required,
//   message: (_, values) => i18n.$t('validations.required', values)
// });
// extend("required", {
//   ...required,
//   message: "{_field_} must be filled"
// });

extend("email", email);
// extend("", {
//   ...email,
//   message: "{_field_} must be filled with valid email format (ie: email@domain.com)"
// });
extend("min", min);
extend("max", max);
extend("min_value", min_value);
extend("numeric", numeric);
