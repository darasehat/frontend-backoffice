import {configure } from "vee-validate";

export default ({ app }) => {
  // Get localized path for homepage
  const localePath = app.localePath('index')
  // Get path to switch current route to French
  const switchLocalePath = app.switchLocalePath('id')

  // beforeLanguageSwitch called right before setting a new locale
  app.i18n.beforeLanguageSwitch = (oldLocale, newLocale) => {
    console.log(oldLocale, newLocale)
  }
  // onLanguageSwitched called right after a new locale has been set
  app.i18n.onLanguageSwitched = (oldLocale, newLocale) => {
    console.log(oldLocale, newLocale)
  }

  configure({
    defaultMessage: (field, values) => {
      values._field_ = app.i18n.t(`${field}`);
      return app.i18n.t(`validation.${values._rule_}`, values);
    }
  });

  // // Set the i18n instance on app
  // // This way we can use it globally in our components through this.$i18n
  // app.i18n = new VueI18n({
  //   // Set the initial locale
  //   locale: "id",

  //   // Set the fallback locale in case the current locale can't be found
  //   fallbackLocale: "id",

  //   // Associate each locale to a content file    
  //   messages: {
  //     en: require("~/static/lang/content-en.json"),
  //     id: require("~/static/lang/content-id.json")
  //   }
  // });
}
