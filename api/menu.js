const Menu = [
  {
    title: 'Dashboard',
    group: 'apps',
    icon: 'dashboard',
    name: 'Dashboard',
    href: '/dashboard',
    permission: 'dashboard'
  },
  {
    header: 'Admin',
    permissions:[
      'user_index',
      'permission_index',
      'role_index',
      'administrativeareafirst_index',
      'administrativeareasecond_index',
      'administrativeareathird_index',
      'administrativeareafourth_index']},
  {
    title: 'Account',
    group: 'accounts',
    component: 'acccounts',
    icon: 'tune',
    active: true,
    permissions: ['user_index','permission_index','role_index'],
    items: [
      {
        name: 'user',
        title: 'User',
        href: '/user',
        permission: 'user_index'
      },
      {
        name: 'permission',
        title: 'Permission',
        href: '/permission',
        permission: 'permission_index'
      },
      {
        name: 'role',
        title: 'Role',
        href: '/role',
        permission: 'role_index'
      },
    ]
  },
  {
    title: 'Master',
    group: 'masters',
    component: 'masters',
    icon: 'tune',
    permissions: [
      'administrativeareafirst_index',
      'administrativeareasecond_index',
      'administrativeareathird_index',
      'administrativeareafourth_index'],
    items: [
      {
        title: 'Area',
        group: 'areas',
        name: 'areas',
        permissions: [
          'administrativeareafirst_index',
          'administrativeareasecond_index',
          'administrativeareathird_index',
          'administrativeareafourth_index'],
        items: [
          {
            name: 'administratif_area_1',
            title: 'Provinsi',
            href: '/layanesia/administrativeareafirst',
            permission: 'administrativeareafirst_index'
          },
          {
            name: 'administratif_area_2',
            title: 'Kabupaten/Kota',
            href: '/layanesia/administrativeareasecond',
            permission: 'administrativeareasecond_index'
          },
          {
            name: 'administratif_area_3',
            title: 'Kecamatan',
            href: '/layanesia/administrativeareathird',
            permission: 'administrativeareathird_index'
          },
          {
            name: 'administratif_area_4',
            title: 'Kelurahan/Desa',
            href: '/layanesia/administrativeareafourth',
            permission: 'administrativeareafourth_index'
          }
        ],
      }
    ]
  },
  {
    title: 'Akun Saya',
    group: 'my_account',
    icon: 'user',
    name: 'my_account',
    href: '/my_account',
    permission: 'my_account_index'
  },
  {header: 'Obat Tambah Darah', permissions: [
    'medicine_record_index','news_index'
  ]},
  {
    title: 'Catatan Obat',
    group: 'medicine_record',
    icon: 'medication',
    name: 'medicine_record',
    href: '/medicine_record',
    permission: 'medicine_record_index'
  },
  {
    title: 'Informasi',
    group: 'news',
    icon: 'article',
    name: 'news',
    href: '/news',
    permission: 'news_index'
  },
  {
    title: 'Pengguna',
    group: 'user',
    component: 'user',
    icon: 'person',
    permissions: ['user_operator_index','end_user_index'],
    items: [
      {
        title: 'Operator', name: 'operator', icon: 'fingerprint', href: '/operator',permission: 'user_operator_index'
      },
      {
        title: 'End User', name: 'end user', href: '/end_user',permission: 'end_user_index'
      }
    ]
  },
  
  /**
   * [header description]
   * @type {String}
  {header: 'Apps'},
  {
    title: 'Chat',
    group: 'apps',
    icon: 'chat_bubble',
    target: '_blank',
    name: 'Chat',
    href: '/chat/messaging'
  },
  {
    title: 'Inbox',
    group: 'apps',
    name: 'Mail',
    target: '_blank',
    icon: 'email',
    href: '/mail/all'
  },
  {
    title: 'Media',
    group: 'apps',
    name: 'Media',
    icon: 'perm_media',
    href: '/media'
  },
  {
    title: 'Widgets',
    group: 'widgets',
    component: 'widgets',
    icon: 'widgets',
    items: [
      {name: 'social', title: 'Social', href: '/widgets/social'},
      {name: 'statistic', title: 'Statistic', badge: 'new', href: '/widgets/statistic'},
      {name: 'chart', title: 'Chart', href: '/widgets/chart'},
      {name: 'list', title: 'List', href: '/widgets/list'},
    ]
  },
  {header: 'UI Elements'},
  {
    title: 'General',
    group: 'components',
    component: 'components',
    icon: 'tune',
    items: [
      {name: 'alerts', title: 'Alerts', href: '/general/alerts'},
      {name: 'avatars', title: 'Avatars', href: '/general/avatars'},
      {name: 'badges', title: 'Badges', href: '/general/badges'},
      {name: 'buttons', title: 'Buttons', href: '/general/buttons'},
      {name: 'cards', title: 'Cards', href: '/general/cards'},
      {name: 'carousels', title: 'Carousels', href: '/general/carousels'},
      {name: 'chips', title: 'Chips', href: '/general/chips'},
      {name: 'dialogs', title: 'Dialogs', href: '/general/dialogs'},
      {name: 'icons', title: 'Icons', href: '/general/icons'},
      {name: 'tables', title: 'Data Tables', href: '/general/tables'},
      {name: 'parallax', title: 'Parallax  image', href: '/general/parallax'},
      {name: 'snackbar', title: 'Snackbar', href: '/general/snackbar'},
      {name: 'progress', title: 'Progress', href: '/general/progress'},
      {name: 'slider', title: 'Slider', href: '/general/sliders'},
      {name: 'tooltip', title: 'Tooltip', href: '/general/tooltips'},
      {name: 'pagination', title: 'Pagination', href: '/general/pagination'},
      {name: 'typography', title: 'Typography', href: '/general/typography'},
      {name: 'color', title: 'Color', href: '/general/colors'},

    ]
  },
  {
    title: 'Pickers',
    group: 'pickers',
    component: 'picker',
    icon: 'filter_vintage',
    items: [
      {name: 'timepicker', title: 'Timepicker', href: '/pickers/timepicker'},
      {name: 'datepicker', title: 'Datepicker', href: '/pickers/datepicker'},

    ]
  },
  {
    title: 'Layout',
    group: 'layout',
    component: 'layout',
    icon: 'view_compact',
    items: [
      {name: 'bottom-sheets', title: 'Bottom panels', component: 'components/bottom-sheets'},
      {name: 'expansion-panels', title: 'Expansion panels', component: 'components/expansion-panels'},
      {name: 'footer', title: 'Footer', component: 'components/footer'},
      {name: 'lists', title: 'Lists', component: 'components/lists'},
      {name: 'jumbotrons', title: 'Jumbotrons', badge: 'new', component: 'components/jumbotrons'},
      {name: 'menus', title: 'Menus', component: 'components/menus'},
      {name: 'tabs', title: 'Tabs', component: 'components/tabs'},
      {name: 'toolbar', title: 'Toolbars', component: 'components/toolbar'},
      {name: 'timeline', title: 'Timeline', component: 'components/timeline'},
    ]
  },
  {
    title: 'Forms & Controls',
    group: 'forms',
    component: 'forms',
    icon: 'edit',
    items: [
      {name: 'basic', title: 'General', href: '/forms/basic-forms'},
      {name: 'selects', title: 'Selects', badge: 'new', href: '/forms/selects'},
      {name: 'selection-controls', title: 'Selection Controls', href: '/forms/selection-controls'},
      {name: 'text-fields', title: 'Text Fields', href: '/forms/text-fields'},
      {name: 'steppers', title: 'Steppers', href: '/forms/steppers'},
      {name: 'editors', title: 'Editors', href: '/forms/editors'},
    ]
  },
  {divider: true},
  {header: 'Extras'},
  {
    title: 'Login',
    group: 'extra',
    icon: 'list',
    href: '/login'
  },
  {
    title: 'Register',
    group: 'extra',
    icon: 'list',
    href: '/register'
  },
  {
    title: 'Empty',
    group: 'extra',
    icon: 'insert_drive_file',
    href: '/empty'
  },
   */
];
// reorder menu
Menu.forEach((item) => {
  if (item.items) {
    item.items.sort((x, y) => {
      let textA = x.title.toUpperCase();
      let textB = y.title.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
  }
});

export default Menu;
