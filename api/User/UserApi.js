import axios from '~/plugins/axios'

export const UserApi = {
  getDataList,
  getDataListPage,
  update,
  show,
  add,
  deleteData
}

async function getDataList () {
  const url = '/user'
  const { data } = await axios.get(url)
  return { data }
}

async function getDataListPage (page) {
  const url = '/user?page=' + page
  const { data } = await axios.get(url)
  return { data }
}

function update (postdata, id) {
  const url = '/user/' + id
  return new Promise((resolve) => {
    axios.put(url, postdata)
      .then((response) => {
        resolve(response)
      })
      .catch(({ response }) => {
      })
  })
}

function edit (id) {
  const url = '/user/' + id + '/edit'
  return new Promise((resolve) => {
    axios.get(url)
      .then((response) => {
        resolve(response)
      })
      .catch(({ error }) => {
        if (error === undefined) {
          resolve(error)
        }
      })
  })
}

function show (id) {
  const url = '/user/' + id
  return new Promise((resolve) => {
    axios.get(url)
      .then((response) => {
        resolve(response)
      })
      .catch(({ error }) => {
        if (error === undefined) {
          resolve(error)
        }
      })
  })
}

function add (postdata) {
  const url = '/user'
  return new Promise((resolve) => {
    axios.post(url, postdata)
      .then((response) => {
        resolve(response)
      })
      .catch(({ response }) => {
        resolve(response)
      })
  })
}

function deleteData (id) {
  const url = '/user/' + id
  return new Promise((resolve) => {
    axios.delete(url)
      .then((response) => {
        resolve(response)
      })
      .catch(({ error }) => {
        if (error === undefined) {
          resolve(error)
        }
      })
  })
}
